title: "Sxmo 1.6.0 upgrade: manual steps required"
date: 2021-11-17
---
Upgrade instructions for successfully installing the as of writing most recent
versions 1.6.0-r0 of postmarketos-ui-sxmo-de-sway or
postmarketos-ui-sxmo-de-dwm
([!2684](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2684)).

## New Global UI Controls

Note that also the global UI controls have changed 1.5.x, most importantly for
upgrading:

| Sxmo Version &nbsp;&nbsp;&nbsp; | Launch Terminal &nbsp;&nbsp;&nbsp; | Toggle virtual keyboard |
|--------------|-----------------|-------------------------|
| 1.5.x        | 2x power key    | 1x power key            |
| 1.6.0        | 3x power key    | 1x volume lower key     |

Full Global UI Controls reference:

* [1.5.x](https://man.sr.ht/~anjan/sxmo-docs-stable/USERGUIDE.md#strongglobal-ui-controlsstrong)
* [1.6.0](https://man.sr.ht/~anjan/sxmo-docs/USERGUIDE.md#strongglobal-ui-controlsstrong)

## Preparation

Before upgrading, make sure your [SSH](https://wiki.postmarketos.org/wiki/SSH)
login works. Enable the daemon to start on boot, start it once, verify that it
works. Replace password auth with
[SSH key](https://wiki.postmarketos.org/wiki/SSH#Replace_password_with_SSH_key)
if you are not planning to disable SSH after these upgrading instructions, and
again, verify that you can login successfully.

#### Figure out which version you are currently running

```
$ apk info -vv | grep postmarketos-ui-sxmo
postmarketos-ui-sxmo-1.5.1-r2 - (X11) Simple X Mobile: Mobile environment ...
```

## Upgrade

As always, upgrade with the <code>-a</code> flag:

```
$ sudo apk upgrade -a
```

### Add your user to the seatd group

```
$ sudo usermod -G seat -a $USER
```

### Recommended packages

UIs in postmarketOS have recommended packages
([_pmb_recommends](https://wiki.postmarketos.org/wiki/UI_specific_package#pmb_recommends)),
which get installed by default in new installations. The difference to regular
dependencies
([depends](https://wiki.alpinelinux.org/wiki/APKBUILD_Reference#depends)) is,
that these can be uninstalled by the user. However, apk does not know about
these recommended packages.

Installations of Sxmo 1.5.x have recommended packages for the DWM version of
Sxmo installed, and these do not automatically get uninstalled when installing
the Sway version. So if you want to have only the Sway version installed,
consider removing them manually:

```
$ sudo apk del feh megapixels-gtk3 xcalc
```

Similarly, the recommended packages for the Sway version are not installed
automatically on upgrade. Install them manually:

```
$ sudo apk add imv megapixels
```

The current list of recommended packages can be found in pmaports.git,
`main/postmarketos-ui-sxmo-de-sway/APKBUILD` and
`main/postmarketos-ui-sxmo-de-dwm/APKBUILD`.

## Finish up

Reboot your device, it should boot successfully into Sxmo 1.6.0.

Related: [sxmo-announce thread](https://lists.sr.ht/~mil/sxmo-announce/%3C31IGOQIT71NT5.2LYA6VY1SCLPK%40stacyharper.net%3E)
